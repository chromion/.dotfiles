local opt = vim.opt
local g = vim.g

-- opt.clipboard = "unnamedplus"
opt.cmdheight = 1

-- Indentline
opt.expandtab = true
opt.shiftwidth = 4
opt.smartindent = true

opt.hidden = true
opt.ignorecase = true

-- Numbers
opt.number = true
opt.relativenumber = true

-- opt.shortmess:append "sI"

opt.signcolumn = "yes"
opt.splitbelow = true
opt.splitright = true
opt.tabstop = 2
opt.softtabstop = 2
opt.termguicolors = true
opt.timeoutlen = 400
opt.undofile = true
opt.swapfile = false
opt.scrolloff=8
opt.hlsearch = false
opt.incsearch = true

-- interval for writing swap file to disk, also used by gitsigns
opt.updatetime = 50

opt.colorcolumn = "80"

g.mapleader = " "

-- netrw
g.netrw_banner = 0
g.netrw_localrmdir = 'rm -r'

-- file extension specific tabbing
vim.cmd([[
autocmd Filetype lua setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype json setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
]])
