vim.g.catppuccin_flavour = "macchiato" -- latte, frappe, macchiato, mocha

require("catppuccin").setup({
  transparent_background = true,
  styles = {
    comments = { "italic" },
  },
})

vim.cmd[[colorscheme catppuccin]]

vim.cmd [[
hi TreesitterContext guibg=NONE ctermbg=NONE
]]

-- vim.g.oceanic_next_terminal_italic = 1
-- vim.g.oceanic_next_terminal_bold = 1
-- vim.cmd [[
-- syntax enable
-- 
-- hi Normal guibg=NONE ctermbg=NONE
-- hi LineNr guibg=NONE ctermbg=NONE
-- hi SignColumn guibg=NONE ctermbg=NONE
-- hi EndOfBuffer guibg=NONE ctermbg=NONE
-- ]]
-- vim.cmd("colorscheme OceanicNext")
-- vim.cmd("hi ColorColumn ctermbg=0 guibg=grey")
