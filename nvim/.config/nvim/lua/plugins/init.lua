local ok, packer = pcall(require, 'packer')
if not ok then
  -- not loaded
  return false
end

return packer.startup(function()
  -- packer can manage itself
  use 'wbthomason/packer.nvim'

  use ({"catppuccin/nvim", as = "catppuccin"})

  -- optimiser
  use 'lewis6991/impatient.nvim'

  -- lua functions
  use {'nvim-lua/plenary.nvim', module = 'plenary'}

  -- popup api
  use 'nvim-lua/popup.nvim'

  -- indent detection
  use "editorconfig/editorconfig-vim"

  -- commenting
  use {
    'numToStr/Comment.nvim', 
    module = {'Comment', 'Comment.api'},
    keys = { "gc", "gb", "g<", "g>" },
    config = function()
      require('plugins.configs.comment')
    end,
  }

  -- context based commenting
  use {'JoosepAlviste/nvim-ts-context-commentstring', after = 'nvim-treesitter'}

  -- syntax highlighting 
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    event = { "BufRead", "BufNewFile" },
    cmd = {
      "TSInstall",
      "TSInstallInfo",
      "TSInstallSync",
      "TSUninstall",
      "TSUpdate",
      "TSUpdateSync",
      "TSDisableAll",
      "TSEnableAll",
    },
    config = function()
      require('plugins.configs.treesitter')
    end,
  }

  -- fuzzy finder
  use {
    'nvim-telescope/telescope.nvim',
    cmd = 'Telescope',
    module = 'telescope',
    config = function()
      require('plugins.configs.telescope')
    end,
  }

  -- fuzzy finder syntax support
  use {
    'nvim-telescope/telescope-fzf-native.nvim',
    after = "telescope.nvim",
    run = 'make',
    config = function()
      require('telescope').load_extension('fzf')
    end,
  }

  -- autoclose tags
  use {'windwp/nvim-ts-autotag', after = 'nvim-treesitter'}

  -- autopairs
  use {
    'windwp/nvim-autopairs',
    event = 'InsertEnter',
    config = function()
      require('plugins.configs.autopairs')
    end,
  }

  -- snippet collection
  use {'rafamadriz/friendly-snippets', opt = true }

  -- snippet engine
  use {
    'L3MON4D3/LuaSnip',
    module = "luasnip",
    wants = "friendly-snippets",
    config = function()
      require('plugins.configs.luasnip')
    end,
  }

  -- snippet completion source
  use {
    'hrsh7th/nvim-cmp',
    event = 'InsertEnter',
    config = function()
      require('plugins.configs.cmp')
    end,
  }

  -- snippet completion source
  use {
    'saadparwaiz1/cmp_luasnip',
    after = 'nvim-cmp',
  }

  -- buffer completion source
  use {
    'hrsh7th/cmp-buffer',
    after = 'nvim-cmp',
  }

  -- path completion source
  use {
    'hrsh7th/cmp-path',
    after = 'nvim-cmp',
  }

  -- lsp completion source
  use {
    'hrsh7th/cmp-nvim-lsp',
    after = 'nvim-cmp',
  }

  -- lsp icons
  use {
    'onsails/lspkind.nvim',
    module = "lspkind",
    config = function()
      require('plugins.configs.lspkind')
    end,
  }

  -- use {
  --   'lukas-reineke/indent-blankline.nvim',
  --   event = "BufRead",
  --   config = function()
  --     require('plugins.configs.indent-line')
  --   end,
  -- }

  -- snippet completion source
  use {
    'saadparwaiz1/cmp_luasnip',
    after = 'nvim-cmp',
  }

  -- buffer completion source
  use {
    'hrsh7th/cmp-buffer',
    after = 'nvim-cmp',
  }

  -- path completion source
  use {
    'hrsh7th/cmp-path',
    after = 'nvim-cmp',
  }

  -- lsp completion source
  use {
    'hrsh7th/cmp-nvim-lsp',
    after = 'nvim-cmp',
  }

  -- lsp icons
  use {
    'neovim/nvim-lspconfig',
    config = function()
      require('plugins.configs.lsp')
    end,
  }

  use {
    'jose-elias-alvarez/null-ls.nvim',
    event = { "BufRead", "BufNewFile" },
    config = function() 
      require('plugins.configs.null-ls')
    end,
  }

  use {
    'stevearc/aerial.nvim',
    config = function() 
      require('plugins.configs.aerial') 
    end,
  }

  use {
    'nvim-neo-tree/neo-tree.nvim',
    branch = "v2.x",
    module = "neo-tree",
    cmd = "Neotree",
    requires = { { "MunifTanjim/nui.nvim", module = "nui" } },
    setup = function() vim.g.neo_tree_remove_legacy_commands = true end,
    config = function()
      require("plugins.configs.neo-tree")
    end,
  }

  use {
    'kyazdani42/nvim-web-devicons',
    event = "VimEnter",
    -- config = function()
      -- require "configs.icons" 
    -- end,
  }
  -- use "nvim-lua/popup.nvim"
  use {
    'stevearc/aerial.nvim',
    config = function() 
      require('plugins.configs.aerial') 
    end,
  }

  -- use {
  --   "ThePrimeagen/git-worktree.nvim",
  --   config = function()
  --     require("plugins.configs.worktree")
  --   end,
  -- }

  -- use "vim-test/vim-test"

  -- use 'MunifTanjim/prettier.nvim' -- prettier plugin for neovim's built-in LSP client

  -- use 'lewis6991/gitsigns.nvim'
  -- use 'dinhhuy258/git.nvim' -- For git blame & browse
  -- use { 'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim' }
  -- use 'f-person/git-blame.nvim'
  use 'tpope/vim-fugitive'

  use {
    'mrjones2014/smart-splits.nvim',
    module = "smart-splits",
    config = function()
      require('plugins.configs.smart-splits')
    end,
  }

  use {
    'nvim-treesitter/nvim-treesitter-context',
    after = 'nvim-treesitter',
    config = function()
      require('plugins.configs.treesitter-context')
    end,
  }
end)
