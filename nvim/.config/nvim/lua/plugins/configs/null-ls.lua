local status_ok, null_ls = pcall(require, "null-ls")
if not status_ok then return end

local sources = { 
  -- null_ls.builtins.formatting.prettier_d_slim,
  -- null_ls.builtins.formatting.prettierd,
  -- null_ls.builtins.formatting.prettier_standard,
}

null_ls.setup({
  sources = sources,
})
