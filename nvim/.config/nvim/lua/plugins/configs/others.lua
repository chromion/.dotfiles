local M ={}

M.autopairs = function()
  local ok1, autopairs = pcall(require, "nvim-autopairs")
  local ok2, cmp_autopairs = pcall(require, "nvim-autopairs.completion.cmp")
  local ok3, cmp = pcall(require, "cmp")

  if not (ok1 or ok2 or ok3) then
    return
  end

  autopairs.setup()
  cmp.event:on( 'confirm_done', cmp_autopairs.on_confirm_done({  map_char = { tex = '' } }))
end

M.comment = function()
  local present, nvim_comment = pcall(require, "nvim_comment")
  if present then
    nvim_comment.setup({
      comment_empty = true,
    })
  end
end

M.luasnip = function()
  local present, luasnip = pcall(require, "luasnip")
  if not present then
    return
  end

  require("luasnip/loaders/from_vscode").load {
    paths = vim.fn.stdpath "config" .. "/snippets",
  }
end

return M
