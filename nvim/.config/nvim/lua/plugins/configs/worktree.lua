local ok, worktree = pcall(require, "git-worktree")
if not ok then
  return
end

worktree.setup()
