local function map(mode, lhs, rhs, opts)
  local options = {noremap = true}
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- telescope
map("n", "<leader>ff", [[<cmd>Telescope find_files<cr>]])
map("n", "<leader>fa", [[<cmd>Telescope find_files hidden=true<cr>]])
map("n", "<leader>cm", [[<cmd>Telescope git_commits<cr>]])
map("n", "<leader>gt", [[<cmd>Telescope git_status<cr>]])
map("n", "<leader>fh", [[<cmd>Telescope help_tags<cr>]])
map("n", "<leader>fw", [[<cmd>Telescope live_grep<cr>]])

-- neoformat
map("n", "<leader>fm", [[<cmd>Neoformat<cr>]])

-- git worktree
map("n", "<leader>gwa", [[<cmd>lua require('telescope').extensions.git_worktree.create_git_worktree()<cr>]])
map("n", "<leader>gwl", [[<cmd>lua require('telescope').extensions.git_worktree.git_worktrees()<cr>]])

-- git
map("n", "<leader>ga", [[<cmd>Git fetch --all<cr>]])
map("n", "<leader>gs", [[<cmd>G<cr>]])
map("n", "<leader>gh", [[<cmd>diffget //3]])
map("n", "<leader>gu", [[<cmd>diffget //2]])

-- harpoon
map("n", "<leader>hm", [[<cmd>lua require("harpoon.mark").add_file()<cr>]])
map("n", "<leader>hc", [[<cmd>lua require("harpoon.mark").clear_all()<cr>]])
map("n", "<leader>hl", [[<cmd>lua require("harpoon.ui").toggle_quick_menu()<cr>]])

map("n", "<leader>ha", [[<cmd>lua require("harpoon.ui").nav_file(1)<cr>]])
map("n", "<leader>hs", [[<cmd>lua require("harpoon.ui").nav_file(2)<cr>]])
map("n", "<leader>hd", [[<cmd>lua require("harpoon.ui").nav_file(3)<cr>]])
map("n", "<leader>hf", [[<cmd>lua require("harpoon.ui").nav_file(4)<cr>]])

map("n", "<leader>t", [[<cmd>TestNearest -strategy=neovim<cr>]])
map("n", "<leader>T", [[<cmd>TestFile -strategy=neovim<cr>]])

-- terminal
map("t", "<Esc>", "<C-\\><C-n>")

-- SymbolsOutline
map("n", "<leader>lS", "[[<cmd>Telescope aerial<cr>]]")

map("n", "<leader>e", "[[<cmd>Neotree toggle<cr>]]")
map("n", "<leader>o", "[[<cmd>Neotree focus<cr>]]")

map("v", "<", "<gv")
map("v", ">", ">gv")

map("v", "J", ":m '>+1<CR>gv=gv")
map("v", "K", ":m '<-2<CR>gv=gv")

map("n", "<C-d>", "<C-d>zz")
map("n", "<C-u>", "<C-u>zz")

-- Better window navigation
-- map("n", "<C-h>", [[<cmd>lua require("smart-splits").move_cursor_left()<cr>]])
-- map("n", "<C-j>", [[<cmd>lua require("smart-splits").move_cursor_down()<cr>]])
-- map("n", "<C-k>", [[<cmd>lua require("smart-splits").move_cursor_up()<cr>]])
-- map("n", "<C-l>", [[<cmd>lua require("smart-splits").move_cursor_right()<cr>]])

-- Resize with arrows
map("n", "<C-Up>", [[<cmd>lua require("smart-splits").resize_up()<cr>]])
map("n", "<C-Down>", [[<cmd>lua require("smart-splits").resize_down()<cr>]])
map("n", "<C-Left>", [[<cmd>lua require("smart-splits").resize_left()<cr>]])
map("n", "<C-Right>", [[<cmd>lua require("smart-splits").resize_right()<cr>]])
