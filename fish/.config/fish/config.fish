# starship prompt
#starship init fish | source

set fish_greeting

# aliases
alias ls "ls -p -G"
alias la "ls -A"
alias ll "ls -l"
alias lla "ll -A"

alias g git
alias vim nvim

alias install "sudo pacman -S"
alias update "sudo pacman -Syu"

set -gx EDITOR nvim

bind \cf 'tmux-sessionizer'

if status is-interactive
    # Commands to run in interactive sessions can go here
end

# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end
